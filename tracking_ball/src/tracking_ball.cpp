#include "ros/ros.h"
#include "std_msgs/String.h"
#include <stdio.h>
#include <sstream>
#include <string>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <cv_bridge/cv_bridge.h>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <image_transport/image_transport.h>
#include "std_msgs/MultiArrayLayout.h"
#include "std_msgs/MultiArrayDimension.h"
#include "std_msgs/Int32MultiArray.h"
#include <dynamic_reconfigure/server.h>
#include <tracking_ball/tracking_ball_paramsConfig.h>


using namespace std;
using namespace cv;
using namespace cv_bridge;

Mat imgOriginal;
float rate;

string command;

ros::Publisher chatter_pub;
image_transport::Publisher pub;
image_transport::Publisher pub_color;

sensor_msgs::ImagePtr image_msg;
sensor_msgs::ImagePtr imagecolor_msg;

bool running = false;

int ballXArray[5];
int ballYArray[5];
int radiusArray[5];

int counter = 0;
int indexOfArray = 0;
int countChecking = 0;
int ballExist = 1;
int circleSize = 0;


int transformedBallX = 0;
int transformedBallY = 0;
int transformedRadius = 0;

int lowH;
int highH;

int lowS;
int highS;

int lowV;
int highV;

typedef struct
{
    int radius;
    int coordX;
    int coordY;
} Ball;


//Calculate avg of an array
int avgCalculate (int* ary, int sizeOfAry)
{
    int sum = 0;

    for (int i = 0; i < sizeOfAry; i++, ary++)
    {
        sum = sum + *ary;

    }

    return sum / sizeOfAry;
}

//Check if an array has the same values
int checkValues(int* ary, int sizeOfAry)
{
    int ctr = 0;


    for (int i = 0; i < sizeOfAry - 1; i++)
    {
        if(*(ary + i) == *(ary + (i + 1)))
        {
            ctr++;
        }
    }
    if(ctr == (sizeOfAry - 1))
    {
        return 1;
    }
    return 0;
}

void callback(tracking_ball::tracking_ball_paramsConfig &config, uint32_t level)
{
    lowH = config.lowH;
    highH = config.highH;
    lowS = config.lowS;
    highS = config.highS;
    lowV = config.lowV;
    highV = config.highV;

    ROS_INFO("Reconfigure Request: %d %d %d %d %d %d",
             config.lowH, config.highH,
             config.lowS, config.highS,
             config.lowV, config.highV);
}

void commandCallback(std_msgs::String comm)
{
    command = comm.data.c_str();
}


void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{

    //ROS_INFO("Stepped to img callback function");
    //ROS_INFO("Params: %d %d %d %d %d %d", lowH, highH, lowS, highS, lowV, highV);
    Ball ball;

    try
    {
        imgOriginal = cv_bridge::toCvShare(msg, "rgb8")->image;
        cv::waitKey(30);

        //Calculate the rate: half of the shorter side
        if(imgOriginal.rows < imgOriginal.cols)
        {
            rate = imgOriginal.rows / 2;

        }
        else if(imgOriginal.rows > imgOriginal.cols)
        {
            rate = imgOriginal.cols / 2;

        }
        else if (imgOriginal.rows == imgOriginal.cols)
        {
            rate = imgOriginal.cols / 2;

        }

        if(running == true)
        {

            //Convert image to HSV
            Mat imgHSV;
            cvtColor(imgOriginal, imgHSV, CV_RGB2HSV);

            //Filter HSV image
            //medianBlur(imgHSV, imgHSV, 5);
            //GaussianBlur(imgHSV, imgHSV, Size(81, 81), 10, 0);

            //Threshold HSV image
            Mat imgThresholded;
            inRange(imgHSV, Scalar(lowH, lowS, lowV), Scalar(highH, highS, highV), imgThresholded);

            //morphological opening and closing
            erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
            dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

            dilate(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
            erode(imgThresholded, imgThresholded, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

            //Detect circles on processed image
            vector<Vec3f> circles;
            HoughCircles(imgThresholded, circles, CV_HOUGH_GRADIENT, 2, imgThresholded.rows, 20, 20, 0, 0);

            if((circles.size() == 0) )
            {
                if(counter != 0)
                {
                    //If there's no circle, the new value is the same as the previous
                    ballXArray[indexOfArray] = ballXArray[indexOfArray-1];
                    ballYArray[indexOfArray] = ballYArray[indexOfArray-1];
                    radiusArray[indexOfArray] = radiusArray[indexOfArray-1];

                    counter++;
                    indexOfArray++;
                }
            }
            else if (circles.size() != 0)
            {

                for(size_t currentCircle = 0; currentCircle < circles.size(); currentCircle++)
                {
                    Point center(cvRound(circles[currentCircle][0]), cvRound(circles[currentCircle][1]));
                    int radTemp = cvRound(circles[currentCircle][2]);

                    circle(imgThresholded, center, 3, Scalar(0, 255, 0), -1, 8, 0);
                    circleSize = circles[currentCircle][2];

                    //Calculate position & size

                    //Add the pos & size to tha array
                    ballXArray[indexOfArray] = center.x;
                    ballYArray[indexOfArray] = center.y;
                    radiusArray[indexOfArray] = radTemp;
                }

                counter++;
                indexOfArray++;

            }

            if(indexOfArray == 5)
            {

                indexOfArray = 0;
                if((checkValues(ballXArray,5) == 1) && (checkValues(ballYArray,5) == 1) &&
                        (checkValues(radiusArray,5) == 1))
                {
                    ballExist = 0;
                    ROS_INFO_STREAM("Oops! There is no ball! :(");
                }
            }

            if(counter >= 5)
            {
                //If the array is full, calculate the avg
                ball.coordX = avgCalculate(ballXArray, 5);
                ball.coordY = avgCalculate(ballYArray, 5);
                ball.radius = avgCalculate(radiusArray, 5);
            }


            //Send messages to stream
            std_msgs::Int32MultiArray ballArray;
            ballArray.data.clear();


            std::stringstream ss;

            if(counter >= 5)
            {


                //Calculate position & size
                transformedBallX = ((imgThresholded.cols/2 - ball.coordX) / rate) * 100;
                transformedBallY = ((imgThresholded.rows/2 - ball.coordY) / rate) * 100;
                transformedRadius = (ball.radius/ rate ) * 100;


                ballArray.data.push_back(transformedBallX);
                ballArray.data.push_back(transformedBallY);
                ballArray.data.push_back(transformedRadius);
                ballArray.data.push_back(ballExist);

                //ROS_INFO_STREAM("[ " << ball.coordX << ", " << ball.coordY << "] | Radius: " << ball.radius);
                ROS_INFO_STREAM("[ " << transformedBallX << ", " << transformedBallY << "] | Radius: " << transformedRadius);
                //Publish to topics
                chatter_pub.publish(ballArray);
                Point center(ball.coordX, ball.coordY);
                circle(imgOriginal, center, ball.radius, Scalar(0,255,0), 3, 8, 0);

                image_msg = CvImage(std_msgs::Header(), "mono8", imgThresholded).toImageMsg();
                imagecolor_msg = CvImage(std_msgs::Header(), "rgb8", imgOriginal).toImageMsg();

                pub.publish(image_msg);
                pub_color.publish(imagecolor_msg);
            }
        }



    }
    catch (cv_bridge::Exception& e)
    {
        ROS_ERROR("Could no convert from '%s' to rgb8 ", msg->encoding.c_str());
    }
}

int main(int argc, char **argv)
{

    ros::init(argc, argv, "BallTracking");
    ros::NodeHandle n;


    ros::Subscriber subCommand = n.subscribe("PlayBallCommand", 1000, commandCallback);

    chatter_pub = n.advertise<std_msgs::Int32MultiArray>("PlayBall", 1000);
    image_transport::ImageTransport it(n);
    pub = it.advertise("camera/image", 1);
    pub_color = it.advertise("camera/colorimage", 1);
    //image_transport::Subscriber subCamera = it.subscribe("ball_cam/image_raw", 1, imageCallback);
    image_transport::Subscriber subCamera = it.subscribe("webcam/image_raw", 1, imageCallback);

    dynamic_reconfigure::Server<tracking_ball::tracking_ball_paramsConfig> server;
    dynamic_reconfigure::Server<tracking_ball::tracking_ball_paramsConfig>::CallbackType f;

    f = boost::bind(&callback, _1, _2);
    server.setCallback(f);


    ros::NodeHandle private_node_handler_("~");
    private_node_handler_.param("lowH", lowH, int(40));
    private_node_handler_.param("highH", highH, int(40));
    private_node_handler_.param("lowS", lowS, int(40));
    private_node_handler_.param("highS", highS, int(40));
    private_node_handler_.param("lowV", lowV, int(40));
    private_node_handler_.param("highV", highV, int(40));

    ROS_INFO("Parameters are set: %d %d %d %d %d %d",lowH,highH, lowS, highS, lowV, highV);

    //std_msgs::String msg;
    int lastX = 0;
    int lastY = 0;

    /*chatter_pub = n.advertise<std_msgs::Int32MultiArray>("PlayBall", 1000);
    image_transport::ImageTransport it(n);
    pub = it.advertise("camera/image", 1);
    pub_color = it.advertise("camera/colorimage", 1);
    image_transport::Subscriber subCamera = it.subscribe("ball_cam/image_raw", 1, imageCallback);
    //image_transport::Subscriber subCamera = it.subscribe("webcam/image_raw", 1, imageCallback);*/

    ros::Rate loop_rate(10);

    bool start = false;



    while (ros::ok())
    {

        if(command == "start")
        {
            running = true;
        }
        else if (command == "stop")
        {
            running = false;
        }


        ros::spinOnce();
        loop_rate.sleep();

        //terminate
        if(command == "exit")
        {
            break;
        }


    }

    return 0;
}
