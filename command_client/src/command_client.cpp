#include "ros/ros.h"
#include "std_msgs/String.h"
#include <stdio.h>
#include <sstream>
#include <string>
#include <iostream>

using namespace std;

void checkCommand(string cmd)
{
	if((cmd != "start") && (cmd != "stop") && (cmd != "exit"))
	{
		ROS_ERROR("Bad command! Please type 'start', 'stop' or 'exit'");
	}
	
}


int main(int argc, char **argv)
{
  std_msgs::String msg;
  string command = "";
    
  ros::init(argc, argv, "command_client");
  ros::NodeHandle n;
  ros::Publisher chatter_pub = n.advertise<std_msgs::String>("PlayBallCommand", 1000);
  ros::Rate loop_rate(10);

  while (ros::ok())
  {
    std_msgs::String msg;
    std::stringstream ss;

    ROS_INFO_STREAM("Command: ");
    getline(cin,command);
	
	checkCommand(command);

    ss << command;
    msg.data = ss.str();
	
    
    ROS_INFO("%s", msg.data.c_str());

    chatter_pub.publish(msg);
    ros::spinOnce();

    loop_rate.sleep();
  }


  return 0;
}
